<?php
/**
 * @file
 * A command to choose the transport layer to use in the current enviornment.
 */

namespace Fortissimo\HPCloud\Command;

/**
 * Choose the proper transport layer to use.
 *
 * The available transport layers are Curl (recommended) and native PHP which is
 * slower.
 */
class ChooseTransportLayer extends \Fortissimo\Command\Base {

  public function expects() {
    return $this->description('Choose a HPCloud library transport option.')
      ->andReturns('The name of the class to use for the transport layer.')
      ;
  }

  /**
   * Choose the transport layer returning the name of the class to use.
   */
  public function doCommand() {
    if (function_exists('curl_init')) {
      return '\HPCloud\Transport\CURLTransport';
    }
    else {
      return '\HPCloud\Transport\PHPStreamTransport';
    }
  }

}