<?php
/**
 * @file
 * Authenticate an account with HP Cloud Identity Services.
 */

namespace Fortissimo\HPCloud\Command;

/**
 * Authenticate an account with HP Cloud Identity Services.
 */
class Authenticate extends \Fortissimo\Command\Base {

  public function expects() {
    return $this->description('Authenticate against HP Cloud.')
      ->usesParam('access', 'Access Key ID')
      ->usesParam('secret', 'Secret Key')
      ->usesParam('tenantId', 'Tenant ID')
      ->usesParam('endpoint', 'Identity Services endpoint URL')
      ->usesParam('verbose', 'Whether or not this is in verbose mode')
        ->whichHasDefault(FALSE)
      ->usesParam('output', 'The console output.')
      ->usesParam('dialog', 'Dialog interaction via the console.')
      ->usesParam('file', 'The file to save account information to.')
      ->andReturns('An \HPCloud\Services\IdentityServices object')
      ;
  }

  public function doCommand() {

    $access = $this->param('access');
    $secret = $this->param('secret');
    $tenantId = $this->param('tenantId');
    $endpoint = $this->param('endpoint');
    $verbose = $this->param('verbose');
    $output = $this->param('output');
    $dialog = $this->param('dialog');

    // Check if we already have the values from the context.
    $check = array(
      'access' => 'your Access Key ID',
      'secret' => 'your Secret Key',
      'tenantId' => 'your Tenant ID',
      'endpoint' => 'the Identity Services URL Endpoint',
    );

    foreach ($check as $name => $text) {
      if (empty($$name)) {
        $$name = $dialog->ask($output, 'Please enter ' . $text . ': ');
        
        // Add the information back into the context so it is available later
        // if needed.
        $this->context->add($name, $$name);
      }
    }

    $ident = new IdentityServices($endpoint);
    
    try {
      $res = $ident->authenticateAsAccount($access, $secret, $tenantId);

      if ($verbose) {
        $output->writeln("<comment>Authentication complete.</comment>");
      }
    } catch (\Exception $e) {
      if ($verbose) {
        $output->writeln("<error>Authentication error: " . $e->getMessage() ."</error>");
      }
    }

    return $ident;
  }
}